﻿using CsvHelper;
using System;
using System.IO;

namespace CsvBuilder
{
    class Program
    {
        static void Main(string[] args)
        {
            var allLines = File.ReadAllLines("crawled.txt");

            Random rand = new Random();

            for (int i = 0; i < 700; i++)
            {
                var lineNum = rand.Next(allLines.Length);
                var lineContent = allLines[lineNum];
                var contents = lineContent.Split(';');
                if(contents[0].Length > 125)
                    continue;
                lineContent = lineContent.Replace("&#039;", string.Empty);
                lineContent = lineContent.Replace("&quot;", string.Empty);
                System.IO.File.AppendAllText(@"products.csv", lineContent + ";1;" + rand.Next(5, 150) + "\n");
            }

            Console.WriteLine("DONE");

            Console.ReadKey();
        }
    }
}
